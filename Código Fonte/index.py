import ServiceFormulas as formulas
import pandas as pd
import HistogramaService as histogramas

materia_default = 'math'
 
conjunto                = pd.read_csv("StudentsPerformance.csv")
service_formulas        = formulas.ServiceFormulas(conjunto)
service_historigramas   = histogramas.HistogramaService(conjunto)

def calculos_media():
    print(f"Cálculos de média, materia: {materia_default.capitalize()} ")
    print(f"Média Simples: Resultado: {service_formulas.media_simples(service_formulas.materias[materia_default])}")
    print(f"Média Aparada: Resultado: {service_formulas.media_aparada(service_formulas.materias[materia_default])}")
    print(f"Media Ponderada: Resultado: {service_formulas.media_ponderada()}")
    print(f"Mediana: Resultado: {service_formulas.mediana(service_formulas.materias[materia_default])}")

def calculos_variancia():
    print("Cálculos de variância: ")
    print(service_formulas.calculo_matriz_correlacao())
    
    print(f"Desvio Absoluto Médio:")
    for item in service_historigramas.materias:
        print(f"Resultado {item.capitalize()}: {service_formulas.desvio_absoluto_medio(service_historigramas.materias[item])}")
    print(f"Variancia: Resultado: {service_formulas.varincia(service_historigramas.materias[materia_default])}")
    
    print("\nDesvio Padrão Matérias:")
    for item in service_historigramas.materias:
        print(f"Resultado {item.capitalize()}: {service_formulas.desvio_padrao(service_formulas.materias[item])}")
    
    print("\nDesvio Absoluto Mediano Mediana")
    for item in service_historigramas.materias:
        print(f"Resultado {item.capitalize()}: {service_formulas.desvio_absoluto_mediano_mediana(service_formulas.materias[item])}")

def histogramas():
    print("\nHistorigramas das Matérias")
    for item in service_historigramas.materias:
        service_historigramas.histograma(service_historigramas.materias[item])

    print("Cálculo da matriz de correlação, Mapa de Calor")
    service_historigramas.matriz_correlacao(service_formulas.calculo_matriz_correlacao())
    print(service_historigramas.calculo_matriz_correlacao())

    service_historigramas.visualizacao_3d_notas_disciplinas(['lunch == "standard"','lunch == "free/reduced"'])
    service_historigramas.visualizacao_3d_notas_disciplinas(['gender == "male"','gender == "female"'])
    service_historigramas.visualizacao_3d_notas_disciplinas(['`race/ethnicity` == "group A"','`race/ethnicity` == "group B"', '`race/ethnicity` == "group C"','`race/ethnicity` == "group D"'])
    service_historigramas.visualizacao_3d_notas_disciplinas(['`parental level of education` == "bachelor\'s degree"','`parental level of education` == "some college"', '`parental level of education` == "master\'s degree"','`parental level of education` == "high school"'])
    service_historigramas.visualizacao_3d_notas_disciplinas(['`test preparation course` == "completed"', '`test preparation course` == "none"'])


print(f"Selecione uma matéria: 1- math, 2-reading, 3 -writing: ")
materia = int(input())
if materia == 1:
    materia_default = 'math'
    print("Math selecionada")
elif materia == 2:
    materia_default = 'reading'
    print("Reading selecionada")
elif materia == 3:
    materia_default = 'writing'
    print("Writing selecionada")
else:
    print("Seleção inválida")
    exit()

def processamento():
    print(f"Selecione o processo, 1-Calculos de Média, 2-Calculos de Variancia, 3-Histogramas")
    processamento = int(input())
    if processamento == 1:
        calculos_media()
    elif processamento == 2:
        calculos_variancia()
    elif processamento == 3:
        histogramas()
    else:
        print("Seleção inválida")
        exit()

processamento()