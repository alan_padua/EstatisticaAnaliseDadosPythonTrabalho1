import pandas
import math
import statistics

from pandas import DataFrame


class FormulasEstatistica:

    def __init__(self):
        self.materias = {
            'math': 'math score',
            'reading': 'reading score',
            'writing': 'writing score'
        }

    def porcentagem(self, valor: int, porcentagem: int):
        valor_calculado = (valor / 100) * porcentagem
        return valor_calculado

    def media_simples(self, conjunto: pandas.DataFrame, materia: str):
        conjunto = conjunto[materia]

        soma_conjunto = conjunto.sum()
        total_conjunto = conjunto.count()
        media_calculada = float(soma_conjunto / total_conjunto)

        return media_calculada

    def media_aparada(self, conjunto: pandas.DataFrame, outlier_porcentagem: int, materia: str):
        conjunto = conjunto[materia]
        p_outlier = self.porcentagem(1000, int(outlier_porcentagem / 2))
        n_quantidade = int(conjunto.count())

        # conjunto ordenado
        conjunto_tratado = conjunto.to_list()
        conjunto_tratado.sort()

        # tratando indice para aparar
        index_inicio_aparado = int(p_outlier + 1)
        index_final_aparado = int(n_quantidade - p_outlier)

        # Soma do conjunto de dados já tratado
        soma_conjunto_aparado = sum(conjunto_tratado[index_inicio_aparado:index_final_aparado])
        valor_calculado = (soma_conjunto_aparado / (n_quantidade - (2 * p_outlier)))

        return valor_calculado

    def media_ponderada(self, conjunto: pandas.DataFrame):
        # Pesos por matéria
        pesos = {
            'peso_math': 5,
            'peso_reading': 3,
            'peso_writing': 3
        }
        pesos_calc = (pesos['peso_math'] + pesos['peso_reading'] + pesos['peso_writing'])

        # Percore a lista e aplica os pesos por matéria
        lista_media_aluno = []
        total_media = 0
        for indice, elemento in conjunto.iterrows():
            score_math    = int(elemento[self.materias['math']])
            score_reading = int(elemento[self.materias['reading']])
            score_writing = int(elemento[self.materias['writing']])

            media_individual = ((score_math * pesos['peso_math']) + (score_writing * pesos['peso_writing']) + (score_reading * pesos['peso_reading'])) / pesos_calc
            lista_media_aluno.append(media_individual)

            total_media = total_media + media_individual

        media_geral = (total_media / len(lista_media_aluno))

        return media_geral

    def mediana_materia(self, conjunto: pandas.DataFrame, materia: str):
        conjunto = conjunto[materia].to_list()
        return self.mediana(conjunto)

    def mediana(self, conjunto: list):
        conjunto_ordenado = conjunto
        conjunto_ordenado.sort()

        total_conjunto = len(conjunto_ordenado)
        mediana = round(total_conjunto / 2)
        valor_calculado = conjunto_ordenado[mediana]

        return valor_calculado

    def desvio_absoluto_medio(self, conjunto: pandas.DataFrame, materia: str):
        media = self.media_simples(conjunto, materia)
        total_conjuto = len(conjunto)
        soma = 0
        for indice, elemento in conjunto.iterrows():
            valor = (elemento[materia] - media)
            soma += valor

        valor_calculado = soma / total_conjuto

        return valor_calculado

    def variancia(self, conjunto: pandas.DataFrame, materia: str):
        media = self.media_simples(conjunto, materia)
        # print(statistics.pvariance(conjunto['math score']))
        total_conjuto = len(conjunto)
        soma = 0.0
        for indice, elemento in conjunto.iterrows():
            valor = float((elemento[materia] - media) ** 2)
            soma += float(valor)

        valor_calculado = (soma / (total_conjuto - 1))
        return valor_calculado

    def desvio_padrao(self, conjunto: pandas.DataFrame, materia: str):
        variancia = self.variancia(conjunto, materia)
        variancia = math.sqrt(variancia)

        # para teste
        # desvio_math = statistics.pstdev(conjunto[self.materias['math']])
        # desvio_reading = statistics.pstdev(conjunto[self.materias['reading']])
        # desvio_writing = statistics.pstdev(conjunto[self.materias['writing']])
        # print(desvio_math)
        # print(desvio_reading)
        # print(desvio_writing)

        return variancia

    def desvio_absoluto_mediano_mediana(self, conjunto: pandas.DataFrame, materia: str):
        mediana = self.mediana_materia(conjunto, materia)
        soma_conjutos = []
        for indice, elemento in conjunto.iterrows():
            soma_conjutos.append(elemento[materia] - mediana)

        valor_calculado = self.mediana(soma_conjutos)

        return valor_calculado

    def calcula_correlacao_coluna(self, matriz, desvio_col1, desvio_col2, media_col1, media_col2, col1, col2):
        soma_linha = 0
        for x in range(len(matriz[col1])):
            soma_linha += ((matriz[col1][x] - media_col1) * (matriz[col2][x] - media_col2))
        r = soma_linha / ((len(matriz['math']) - 1)* (desvio_col1 * desvio_col2))

        return r

    def calculo_matriz_correlacao(self, conjunto: pandas.DataFrame):

        dicionario = {
            'math': [self.media_simples(conjunto, self.materias['math']), self.desvio_padrao(conjunto, self.materias['math'])],
            'reading': [self.media_simples(conjunto, self.materias['reading']), self.desvio_padrao(conjunto, self.materias['reading'])],
            'writing': [self.media_simples(conjunto, self.materias['writing']), self.desvio_padrao(conjunto, self.materias['writing'])]
        }

        matriz = {
            'math': conjunto[self.materias['math']],
            'reading': conjunto[self.materias['reading']],
            'writing': conjunto[self.materias['writing']]
        }

        matriz_calculada = {}
        # Math
        item = []
        for x in dicionario:
            item = []
            for y in dicionario:
                item.append(self.calcula_correlacao_coluna(matriz, dicionario[x][1], dicionario[x][1], dicionario[y][0], dicionario[y][0], x, y))
            matriz_calculada[x] = item

        return matriz_calculada
