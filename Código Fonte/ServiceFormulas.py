import statistics
import FormulasEstatistica as formulas_estatistica
import pandas as pd
import numpy as np
import wquantiles

class ServiceFormulas:
    def __init__(self, conjunto: pd.DataFrame):
        self.conjunto = conjunto
        self.formula = formulas_estatistica.FormulasEstatistica()
        self.materias = {
            'math': 'math score',
            'reading': 'reading score',
            'writing': 'writing score'
        }

    # Cálculos de média
    def media_simples(self, materia: str):
        valor_calculado = self.formula.media_simples(self.conjunto, materia)
        return valor_calculado

    # Média aparada
    def media_aparada(self, materia: str):
        valor_calculado = self.formula.media_aparada(self.conjunto, 10, materia)
        return valor_calculado

    # Média ponderada
    def media_ponderada(self):
        valor_calculado = self.formula.media_ponderada(self.conjunto)
        return valor_calculado

    # Mediana (justifique suas escolhas de cada parâmetro) para cada disciplina;
    def mediana(self, materia: str):
        valor_calculado = self.formula.mediana_materia(self.conjunto, materia)
        return valor_calculado

    def desvio_absoluto_medio(self, materia: str):
        valor_calculado = self.formula.desvio_absoluto_medio(self.conjunto, materia)
        return valor_calculado

    def varincia(self, materia: str):
        valor_calculado = self.formula.variancia(self.conjunto, materia)
        return valor_calculado

    def desvio_padrao(self, materia: str):
        valor_calculado = self.formula.desvio_padrao(self.conjunto, materia)
        return valor_calculado

    def desvio_absoluto_mediano_mediana(self, materia: str):
        valor_calculado = self.formula.desvio_absoluto_mediano_mediana(self.conjunto, materia)
        return valor_calculado

    def calculo_matriz_correlacao(self):
        corr_matrix = self.formula.calculo_matriz_correlacao(self.conjunto)
        return corr_matrix