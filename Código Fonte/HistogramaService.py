from pandas import DataFrame

import FormulasEstatistica as formulasEstatistica
import pandas as pd
import matplotlib.pyplot as plt
import plotly.express as px
import seaborn as sn
import random
import numpy as np

import seaborn as sn


class HistogramaService:

    def __init__(self, conjunto: pd.DataFrame):
        self.conjunto = conjunto
        self.formula = formulasEstatistica.FormulasEstatistica()
        self.materias = {
            'math': 'math score',
            'reading': 'reading score',
            'writing': 'writing score'
        }

    def histograma(self, materia: str):
        #fig = px.histogram(self.conjunto, x=materia)
        # fig.show()
        plt.hist(self.conjunto[materia], bins='auto')
        plt.title(f"Histogram score '{materia.replace('score','')}' ")
        plt.show()

    def calculo_matriz_correlacao(self):
         corr_matrix = self.conjunto.corr()
         sn.heatmap(corr_matrix, annot=True)
         plt.show()
         return

    def visualizacao_3d_notas_disciplinas(self, lista_filtro: list):
        fig = plt.figure(figsize=(20, 10))
        ax = plt.axes(projection='3d')

        # print(self.conjunto.head(3))
        for filtro in lista_filtro:
            marker = np.random.choice([1, 2, 3, 4, 5, 6, 7, 8, 9])
            # print(marker)
            conjunto_filtrado = self.conjunto.query(filtro)
            # Creating plot
            rgb = np.random.rand(3, )
            ax.scatter3D(conjunto_filtrado['math score'], conjunto_filtrado['writing score'],
                         conjunto_filtrado['reading score'], c=[rgb], marker=marker, label='adasd')

        plt.title(f" Filtro: {filtro.replace('==', '=').replace('`','')}")

        # axx = plt.Axes();
        # axx.set_facecolor('red')

        ax.set_xlabel('math score')
        ax.set_ylabel('writing score')
        ax.set_zlabel('reading score')

        plt.show()

    def matriz_correlacao(self, conjunto: dict):
        matriz_calculada = DataFrame(data=conjunto)
        sn.heatmap(matriz_calculada, annot=True)
        plt.show()

        return
