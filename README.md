# README
## Informações Básicas


## Apresentação

A apresentação do trabalho se encontra na pasta **Apresentação**, dentro da pasta contém os arquivos:

 - Apresentacao.mkv = Gravação da Apresentação
 - Trabalho 01.pdf = O pdf uttilizado na aprensentação gravada.
 -  Enunciado.txt = O enunciado original dos requisitos do projeto.

## Código Fonte

O código fonte está na pasta **Código Fonte**, para inicializar o projeto é necessário estar dentro da pasta do **código fonte**.

Arquivos da pasta:
- index.py = Arquivos de inicialização da aplicação
- HistogramaService.py = Biblioteca para apresentação dos gráficos.
- FormulasEstatistica.py = Biblioteca com as formulas de media e variância
- ServiceFormulas.py = Serviço para chamada das fórmulas.
- requirements.txt = Versão das dependências do projeto.

Arquivos de inicio do projeto é **index.py**

**Comando para iniciar o projeto:**

	python3.8 index.py
	
### Ambiente de desenvolvimento

O trabalho foi feito localmente utilizando o VScode, com o Python 3.8.5.

### Requisitos dos Sistema

**Sistema operacional:**  Linux mint

### Dependências

Dependências necessárias para executar o projeto.
 - pandas==1.1.4
 - numpy==1.19.4 
- wquantiles==0.5 
- matplotlib==3.3.3 
- plotly==4.14.3
- seaborn==0.11.1

Estou utilizando o **pip** para instalar as dependências:
**Comando para instalar o pip:**

	sudo apt install python3-pip

**Comandos para instalação da dependências:**

	pip3 install pandas
	pip3 install wquantiles
	pip3 install matplotlib
	pip3 install plotly
	pip3 install seaborn



